package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 自定义定时任务，实现订单状态定时处理
 */
@Component
@Slf4j
public class OrderTask {

    @Autowired
    private OrdersMapper ordersMapper;

    /**
     * 处理支付超时订单
     * 超时十五分钟就自动取消
     */
   // @Scheduled(cron = "0 * * * * ?")
    // @Scheduled(cron = "0/5 * * * * ?") //测试例子 每五秒提醒一次
    public void noPay(){
        log.info("处理支付超时订单：{}", LocalDateTime.now());
        //当前时间减去十五分钟
        LocalDateTime time = LocalDateTime.now().plusMinutes(-15);
        // select * from orders where status = 1 and order_time < 当前时间-15分钟
        List<Orders> ordersList = ordersMapper.getByStatusAndOrdertimeLT(Orders.PENDING_PAYMENT, time);
        if(ordersList != null && ordersList.size() > 0){
            for (Orders orders : ordersList) {
                //拼装数据 对原本数据进行修改
                orders.setStatus(Orders.CANCELLED);
                orders.setCancelReason("支付超时，自动取消");
                orders.setCancelTime(LocalDateTime.now());
                ordersMapper.update(orders);
            }
        }
    }

    /**
     * 处理“派送中”状态的订单
     * 凌晨一点对还处于派送中的订单进行修改为已完成
     */
   // @Scheduled(cron = "0 0 1 * * ?")
   // @Scheduled(cron = "0/5 * * * * ?") //测试例子 每五秒提醒一次
    public void noComplete(){
        log.info("处理派送中订单：{}");
        LocalDateTime time = LocalDateTime.now().plusMinutes(-60);
        // select * from orders where status = 1 and order_time < 当前时间-60分钟
        List<Orders> ordersList = ordersMapper.getByStatusAndOrdertimeLT(Orders.DELIVERY_IN_PROGRESS, time);
        if(ordersList != null && ordersList.size() > 0){
            for (Orders orders : ordersList) {
                //拼装数据 对原本数据进行修改
                orders.setStatus(Orders.COMPLETED);
                ordersMapper.update(orders);
            }
        }
    }

}