package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理
 * http://localhost:8080/admin/employee/login
 */
@RestController
@RequestMapping("/admin/employee")
@Slf4j
@Api(tags = "员工相关接口")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("用户登录")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //登录成功后，生成jwt令牌  header.payload.数字签名（header.payload.secret+算法）
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),
                jwtProperties.getAdminTtl(),
                claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 退出
     *
     * @return
     */
    @PostMapping("/logout")
    @ApiOperation("用户退出")
    public Result<String> logout() {
        return Result.success();
    }

    /**
     * 新增员工
     * @param employeeDTO
     * @return
     */
    @ApiOperation("新增员工")
    @PostMapping
    public Result insert(@RequestBody EmployeeDTO employeeDTO){
        System.out.println("线程id:"+Thread.currentThread().getId());
        //1.接收数据
        log.info("新增员工传参：{}",employeeDTO);
        //2.调用service层，实现业务逻辑处理
        employeeService.insert(employeeDTO);
        //3.响应数据给前端
        return Result.success();
    }

    /**
     * 员工分页查询
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("员工分页查询")
    // public Result<PageResult> page(String name,Integer page,Integer pageSize){
    public Result<PageResult> page(EmployeePageQueryDTO employeePageQueryDTO){
        //1.接收数据
        log.info("员工分页查询传参：{}",employeePageQueryDTO);
        //2.调用service层，实现业务逻辑处理
        PageResult pageResult = employeeService.page(employeePageQueryDTO);
        //3.响应数据给前端
        return Result.success(pageResult);
    }


    /**
     * 启用或者禁用账号
     * @param status
     * @param id
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation("启用或者禁用账号")
    public Result updateStatus(@PathVariable Integer status,Long id){
        //1.接收数据
        log.info("启用或者禁用账号传参：{},{}",status,id);
        //2.调用service层，实现业务逻辑处理
        employeeService.updateStatus(status,id);
        //3.响应数据给前端
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id获取员工信息")
    public Result<Employee> getById(@PathVariable Long id){
        //1.接收数据
        log.info("id传参：{}",id);
        //2.调用service层，实现业务逻辑处理
        Employee employee = employeeService.getById(id);
        //3.响应数据给前端
        return Result.success(employee);
    }

    @PutMapping
    @ApiOperation("修改员工信息")
    public Result updateById(@RequestBody EmployeeDTO employeeDTO){
        //1.接收数据
        log.info("修改传参：{}",employeeDTO);
        //2.调用service层，实现业务逻辑处理
        employeeService.updateById(employeeDTO);
        //3.响应数据给前端
        return Result.success();
    }


}
